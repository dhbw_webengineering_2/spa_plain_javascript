async function buildListView() {
    
}

function buildTodoListTitle(title) {
    const todoListTitle = document.createElement('span');

    todoListTitle.innerText = title;
    todoListTitle.className = 'todo-list-item-title';

    return todoListTitle;
}

function buildTodoListCheckbox(checked) {
    const todoListCheckbox = document.createElement('input');

    todoListCheckbox.type = 'checkbox';
    todoListCheckbox.disabled = true;
    todoListCheckbox.value = checked;
    todoListCheckbox.className = 'todo-list-item-checkbox';

    return todoListCheckbox;
}

function buildTodoListDetailButton(navigateToDetail) {
    const todoListDetailButton = document.createElement('a');

    todoListDetailButton.innerText = 'Detailansicht'
    todoListDetailButton.onclick = navigateToDetail;
    todoListDetailButton.className = 'todo-list-item-button button1';

    return todoListDetailButton;
}

function buildTodoListRow() {
    const todoListRow = document.createElement('div');

    todoListRow.className = 'todo-list-item';

    return todoListRow;
}
