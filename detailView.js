async function buildDetailView({ todoId }) {
    
}

function buildTodoDetailTitle(title) {
    const todoDetailTitle = document.createElement('h2');

    todoDetailTitle.innerText = title;
    todoDetailTitle.className = 'todo-detail-title';

    return todoDetailTitle;
}

function buildTodoDetailDescription(description) {
    const todoDetailDescription = document.createElement('p');

    todoDetailDescription.innerText = description;

    return todoDetailDescription;
}

function buildTodoDetailBackgroundContainer() {
    const detailViewContainer = document.createElement('div');

    detailViewContainer.className = 'todo-detail-background';
    
    return detailViewContainer;
}